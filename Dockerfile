# базовый образ
FROM python:3.6-jessie

# откуда куда копировать файлы в образ
COPY ./ /usr/src/app/gym-gpn

# рабочая директория
WORKDIR /usr/src/app/gym-gpn

# установка gym-gpn
RUN pip install -e . --user

# запуск агента (нужно указать актуальный путь к разработанному агенту)
CMD [ "python", "-u", "./examples/agents/random_walk.py" ]
