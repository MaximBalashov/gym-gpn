import numpy as np
from time import sleep

token = '1195f101-dc79-4be4-8812-bd6497b91fbc'


class Agent:
    '''
    Шаблон агентной модели для среды Gym-GPN
    '''
    def __init__(self, observations=None):
        self.observations = observations or []

    def act(self):
        raise NotImplementedError


class RandomWalkAgent(Agent):
    '''
    Агентная модель для среды Gym-GPN со случайным выбором цены
    '''
    def __init__(self, low_price, high_price):
        self.low_price = low_price
        self.high_price = high_price


    def act(self):
        return np.round(
            np.random.randint(
                np.ceil(self.low_price),
                np.ceil(self.high_price)
            ), 2)


if __name__ == '__main__':

    import gym
    import gym_gpn

    env = gym.make('gpn-v0')
    env.create_thread(token=token)
    agnt = RandomWalkAgent(env.low_price, env.high_price)

    done = env.done
    while True:
        price = agnt.act()
        state, reward, done, _  = env.step(price)
        if done:
            break
        print(str(state['date_time']) + ',' + str(env.cum_reward) + ',' + str(price))
